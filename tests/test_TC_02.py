from  base_test_case import BaseTestCase

class TestPermsChange_mounted(BaseTestCase):

    def test_PermsChange_mounted(self):
        fileName = self.genFilename()
        newMode = '0777'
        src  = self.srcPath + fileName
        dst = self.dstPath + fileName
        self.createFile(self.dstPath, fileName)
        assert self.compareFileMode(src, dst)
        self.changeFilePerms(dst, 0777)
        assert self.compareFileMode(src, dst)
        assert self.getFileAttr(src) == newMode
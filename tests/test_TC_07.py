from  base_test_case import BaseTestCase


class TestOwnerChange_src(BaseTestCase):

    def test_OwnerChange_src(self):
        fileName = self.genFilename()
        newGrp = 'tuser1'
        src = self.srcPath + fileName
        dst = self.dstPath + fileName
        self.createFile(self.srcPath, fileName)
        self.changeFileGrp(src, newGrp)
        assert self.compareFileGrp(src, dst)
        for i in (src, dst):
            assert self.checkFileGrp(i, newGrp)
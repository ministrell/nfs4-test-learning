from  base_test_case import BaseTestCase


class TestOwnerChange_mounted(BaseTestCase):

    def test_OwnerChange_mounted(self):
        fileName = self.genFilename()
        newOwner = 'tuser1'
        src = self.srcPath + fileName
        dst = self.dstPath + fileName
        self.createFile(self.dstPath, fileName)
        self.changeFileOwner(dst, newOwner)
        assert self.compareFileMode(src, dst)
        for i in (src, dst):
            assert self.checkFileOwner(i, newOwner)
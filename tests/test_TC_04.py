from  base_test_case import BaseTestCase


class TestFileCreation_source(BaseTestCase):

    def test_FileCreation_mounted(self):
        fileName = self.genFilename()
        srcPath = self.getConfigAttr('Paths', 'srcPath')
        dstPath = self.getConfigAttr('Paths', 'dstPath')
        self.createFile(srcPath, fileName)
        assert self.compareFileContent(srcPath + fileName, dstPath + fileName)
from  base_test_case import BaseTestCase


class TestFileCreation_mounted(BaseTestCase):

    def test_FileCreation_mounted(self):
        fileName = self.genFilename()
        srcPath = self.getConfigAttr('Paths', 'srcPath')
        dstPath = self.getConfigAttr('Paths', 'dstPath')
        self.createFile(dstPath, fileName)
        assert self.compareFileContent(srcPath + fileName, dstPath + fileName)
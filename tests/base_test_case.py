import unittest
import sys
import os
import time
import pwd
import grp
import shutil
import ConfigParser
import filecmp
import subprocess




class BaseTestCase(unittest.TestCase):

    config = ConfigParser.ConfigParser()
    config.readfp((open(r'config/testing.ini')))
    srcPath = config.get('Paths', 'srcPath')
    dstPath = config.get('Paths', 'dstPath')

    # users = ['tuser1','tuser2']

    @classmethod # we need to use this decorator  to be able to user method before class object initiation - to make cleaning before starting tests
    def nfsPathsCheck(cls, srcPath, dstPath):  # check what testing directories are empty
        for targDir in srcPath, dstPath:
            if not os.path.exists(targDir):  # check directories exists. if not - creates
                os.makedirs(targDir)
            while os.listdir(targDir):  # while directory is not empty  - keep trying to clean it
                for targFile in os.listdir(targDir):
                    filePath = os.path.join(targDir, targFile)
                try:
                    if os.path.isfile(filePath):  # for files
                        os.unlink(filePath)
                    elif os.path.isdir(filePath):
                        shutil.rmtree(filePath)  # for folders
                except Exception as e:
                    print(e)
                    quit(9)


    def createFile(self, dstPath, fileName):
        shutil.copyfile('mockups/file_mockup', dstPath + fileName)

    def changeFilePerms(self, path, new_mode):
        os.chmod(path, new_mode)

    def changeFileOwner(self, path, newOwner):
        subprocess.call(['sudo', 'chown', newOwner, path])

    def changeFileGrp(self, path, newGrp):
        subprocess.call(['sudo', 'chgrp', newGrp, path])

    def compareFileContent(self, file1, file2):
        isEqual = filecmp.cmp(file1, file2)
        return isEqual

    def compareFileMode(self, file1, file2):
        isEqual = False
        if (os.stat(file1).st_mode == os.stat(file2).st_mode):
            isEqual = True
        else:
            for i in file1, file2:
                print "file %s mode %s" %(i, os.stat(i).st_mode)
        return isEqual

    def compareFileOwner(self, filePath1, filePath2):
        isEqual = False
        def owner(file):
            owner = str(os.stat(file).st_uid) + ":" + str(os.stat(file).st_gid)
        if owner(filePath1) == owner(filePath2):
            isEqual = True
        else:
            for i in filePath1, filePath2:
                print " %s owner %s" %(i, owner(i))
        return isEqual

    def compareFileGrp(self, filePath1, filePath2):
        isEqual = False
        def grp(file):
             return str(os.stat(file).st_gid) + ":" + str(os.stat(file).st_gid)
        if grp(filePath1) == grp(filePath2):
            isEqual = True
        else:
            for i in filePath1, filePath2:
                print "%s grp %s" %(i, grp(i))
        return isEqual



    def checkFileOwner(self, filePath, expectedOwner):
        isEqual = False
        ownUID = os.stat(filePath).st_uid
        if ownUID == pwd.getpwnam(expectedOwner).pw_uid:
            isEqual = True
        else:
            print "%s expected owner is %s but actual is %s" %(filePath, expectedOwner,pwd.getpwuid(ownUID).pw_name)
        return isEqual

    def checkFileGrp(self, filePath, expectedGrp):
        isEqual = False
        ownGID = os.stat(filePath).st_gid
        if ownGID == grp.getgrnam(expectedGrp).gr_gid:
            isEqual = True
        else:
            print "%s expected group is %s but actual is %s" %(filePath, expectedGrp,pwd.getpwuid(ownGID).gr_name)
        return isEqual

    def getFileAttr(self, path):
        st_mode = os.stat(path).st_mode
        mode = oct(st_mode)[3:]  # converting st_mode to file mode
        return mode

    def getConfigAttr(self, section, attr):
       return self.config.get(section, attr)

    def getUserFileACL(self, filePath, username):
        textacl = subprocess.check_output(["getfacl",filePath]).split('\n')[3:6]
        acl = {}
        for i in textacl:
            if "user" in i & username in i:
                acl[i.split(":")[1]] = i.split(":")[2]
        return acl

    def getUserNFSFileACL(self, filePath, username):
        textacl = subprocess.check_output(["nfs4_getfacl",filePath]).split('\n')
        acl = []
        for i in textacl:
            if username in i:
                acl[i.split(":")[2]] = i.split(":")[3]
        return acl



    def genFilename(self):
        fileName = time.strftime("%d%b%Y-%H:%M:%S", time.localtime())
        return fileName

    @classmethod  # run cleaning before running test
    def setUpClass(cls):
        cls.nfsPathsCheck(cls.srcPath, cls.dstPath)

    @classmethod
    def tearDownClass(cls): # run cleaning after runing tests
       cls.nfsPathsCheck(cls.srcPath, cls.dstPath)
    #     #print "TearDownClassMethod done"


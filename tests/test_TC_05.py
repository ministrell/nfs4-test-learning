from  base_test_case import BaseTestCase
from time import sleep


class TestOwnerChange_src(BaseTestCase):

    def test_OwnerChange_src(self):
        fileName = self.genFilename()
        newMode = '0777'
        src  = self.srcPath + fileName
        dst = self.dstPath + fileName
        self.createFile(self.dstPath, fileName)
        assert self.compareFileMode(src, dst)
        self.changeFilePerms(src, 0777)
        sleep(3)  #seems what permissions need some time to be updated. Strange case
        assert self.compareFileMode(dst, src)
        assert self.getFileAttr(src) == newMode

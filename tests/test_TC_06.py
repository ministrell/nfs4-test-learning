from  base_test_case import BaseTestCase


class TestOwnerChange_src(BaseTestCase):

    def test_OwnerChange_src(self):
        fileName = self.genFilename()
        newOwner = 'tuser1'
        src = self.srcPath + fileName
        dst = self.dstPath + fileName
        self.createFile(self.srcPath, fileName)
        self.changeFileOwner(src, newOwner)
        assert self.compareFileMode(src, dst)
        for i in (src, dst):
            assert self.checkFileOwner(i, newOwner)
import sys
import os
import time
import logging
import pwd
import grp
import unittest
import shutil



#remove to config in future
logpath = "log"
users = {'tuser1':'tuser1', 'tuser2':'tuser2'}
srcPath = '/projects/nfs4/nfs_src/'
dstPath = '/projects/nfs4/nfs_dest/'

def main():
    logPath(logpath)
   # checkUsers(users)
    nfsPathsCheck(srcPath,dstPath)
    nfsPathsMountCheck(srcPath, dstPath)
    testsRun()
#this method finds specifized test  or all  tests in tests directiry  and runs it/them
def testsRun():
    print "Running tests \n"
    pattern = sys.argv[1] if len(sys.argv) == 2 else 'test*.py'
    tests = unittest.defaultTestLoader.discover('tests', pattern)
    if tests.countTestCases():
        unittest.runner.TextTestRunner(verbosity=2).run(tests)

#configures logging for tests
def logPath(logpath):
    # logname = (time.strftime("%d%b%Y-%H:%M:%S", time.localtime()) + '.log')
#checking if log path exists (important because of logs are not added to the repository)
    if not os.path.exists(logpath):
        os.makedirs(logpath)
    # with open(logpath + "/" + logname, 'w+') as log_file:
    #     return log_file
#logfile name format
    logname = time.strftime("%d%b%Y-%H:%M:%S", time.localtime())
    logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s', level=logging.DEBUG, filename=('log/' + logname + '.log'))


#checks what tests users are added to system and  configured correctly
def checkUsers(users):
    usersCreatedCorrectly = ''
    for i in users.iterkeys():
        try:
            pwd.getpwnam(i)
            grp.getgrnam(users[i])
            expGid = grp.getgrnam(users[i]).gr_gid
            setGid = pwd.getpwnam(i).pw_gid
            if not  setGid == expGid:
                print "For user % expected gid=% but set=%" %(expGid,setGid)
                usersCreatedCorrectly = False
        except KeyError:
            usersCreatedCorrectly = False
            print "Test user %s:%s not found or not configured properly" % (i, users[i])
    if usersCreatedCorrectly == False:
        print "WARNING: Testing users should be created manually before running tests!"
        quit(9)

#checks what testing paths are created and empty
#if not empty - clean it
def nfsPathsCheck(srcPath, dstPath):
    for targDir in srcPath, dstPath:
        if not os.path.exists(targDir): #check directories exists. if not - creates
            os.makedirs(targDir)
        while os.listdir(targDir):  #while directory is not empty  - keep trying to clean it
            for targFile in os.listdir(targDir):
                filePath = os.path.join(targDir, targFile)
            try:
                if os.path.isfile(filePath): # for files
                        os.unlink(filePath)
                elif os.path.isdir(filePath): shutil.rmtree(filePath) #for folders
            except Exception as e:
                print(e)
                quit(9)


# checks what source and dest dirs are mounted properly
def nfsPathsMountCheck(srcPath, dstPath):
    isMounted = False
    dstAbsPath = os.path.abspath(dstPath)
    srcAbsPath = os.path.abspath(srcPath)
    with open("/proc/mounts", "r") as mountList:
        for mountStr in mountList:
#            if (mountStr.find(srcAbsPath) != - 1) & (mountStr.find(dstAbsPath) != -1 ):  # This was modified because of nfs v4 mount specific. Source folder looks like
            if  (mountStr.find(dstAbsPath) != -1 ):
                isMounted = True
    if isMounted:
        print "Sourcepath %s is correctly mounted to %s" %(srcAbsPath, dstAbsPath)
    else:
        print "Sourcepath %s is not mounted to %s" %(srcAbsPath, dstAbsPath)
        quit(9)



if __name__ == '__main__':
    main()

○ Name: Modifying file content and owner in source folder


○ Description: Check file and file's owner modified in source folder also modified in mounted folder


○ Steps:
Create file in source folder
Check attributes
Compare file's content and attributes in source and mounted folder
Modify owner
Compare file's content and attributes in source and mounted folder

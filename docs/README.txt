Target of project: check cases for owner/permission/content modification testing of NFS4 file system



WARNING: Testing environment requirements:

SYSTEM MUST BE CONFIGURED BEFORE RUNNING TESTS:

1) User management:
1.1) Users tuser1, tuser2 and their base groups should be presented in system and will not be created automatically.
1.2) tuser1 is expected to be in the same group as user which runs tests
     tuser2 is expected to be in the different group


2) NFS4 settings:
2.1) It is unsafe to run untrusted applications (such as this one) with root privileges, so NFS4 should be configured and mounted manually BEFORE runnign tests.
 Test only checks target and source directory are mounted correctly.
 HOW TO PREPARE NFS4 SHARE FOR TESTING:
 install
    NFSv4 client: sudo apt-get install nfs-common
    NFSv4 server: sudo apt-get install nfs-kernel-server
    NFSv$ acl support: sudo apt-get install nfs4-acl-tools

To export src to a localhost
add the following line to /etc/exports:
/path_to_export       127.0.01(rw,sync,fsid=0,crossmnt,no_subtree_check,no_root_squash, acl)

Warning! no_root_squash options is important because of if it is not specified you will be not able to change owner in mounted folder on remote system

Then run:
    sudo service nfs-kernel-server restart

To mount folder:
    sudo mount -t nfs4 127.0.0.1:/ /mountpoint

3) For automated testing of owner changing code has to have root privileges
If it is necessary you have to add following info in /etc/sudoers file for testing time:
(list of commands in alias will be modified during develop)
WARNING! REPLACE curret_user to your username


Cmnd_Alias  NFS_TESTING = /bin/chown, /bin/chgrp
current_user ALL=(ALL) NOPASSWD: NFS_TESTING

or

WARNING: it is absolutely insecure but where are no another simple way






Each class in code is a test suite which consists of  tests presented as  methods.

Test cases are grouped into  test suites in dependency  of testing subject.


You can run your tests like this:

cd '/path/to/project'
python test.py


one test also  can be runned:

python test.py test_TC_02.py